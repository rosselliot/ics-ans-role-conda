import pytest
import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_conda_version(host):
    cmd = host.run("/opt/conda/bin/conda --version 2>&1")
    assert cmd.rc == 0
    assert cmd.stdout.strip() == "conda 4.8.4"


def test_conda_path(host):
    cmd = host.run('su -l -c "type conda" root')
    assert cmd.stdout.startswith("conda is a function")


def test_condarc_file(host):
    condarc = host.file("/opt/conda/.condarc")
    if (
        host.ansible.get_variables()["inventory_hostname"]
        == "ics-ans-role-conda-rpm"
    ):
        assert (
            condarc.content_string.strip()
            == """channel_alias: https://artifactory.esss.lu.se/artifactory/api/conda
channels:
  - conda-forge
  - anaconda-main
default_channels: []
auto_update_conda: False
use_only_tar_bz2: True"""
        )
    else:
        assert (
            condarc.content_string.strip()
            == """channel_alias: https://artifactory.esss.lu.se/artifactory/api/conda
channels:
  - conda-e3
  - ics-conda-forge
  - conda-forge
  - anaconda-main
default_channels: []
auto_update_conda: False
use_only_tar_bz2: True"""
        )


def test_condaz_default_env_file(host):
    condaz_default_env = host.file("/etc/profile.d/condaz_default_env.sh")
    if (
        host.ansible.get_variables()["inventory_hostname"]
        == "ics-ans-role-conda-create-env"
    ) or (
        host.ansible.get_variables()["inventory_hostname"]
        == "ics-ans-role-conda-create-env-ess-linux"
    ):
        assert (
            condaz_default_env.content_string.strip()
            == r"""export PS1='[\u@\h \W]\$ '
conda activate python36"""
        )
    else:
        assert not condaz_default_env.exists


@pytest.mark.parametrize("name", ["/opt/conda", "/opt/conda/envs", "/opt/conda/pkgs"])
def test_conda_owner(host, name):
    if (
        host.ansible.get_variables()["inventory_hostname"]
        == "ics-ans-role-conda-create-env"
    ) or (
        host.ansible.get_variables()["inventory_hostname"]
        == "ics-ans-role-conda-create-env-ess-linux"
    ):
        user = "vagrant"
        group = "vagrant"
    else:
        user = "root"
        group = "root"
    assert host.file(name).user == user
    assert host.file(name).group == group
